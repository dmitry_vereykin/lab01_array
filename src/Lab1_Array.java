/**
 * Created by Dmitry Vereykin on 9/8/2015.
 */
public class Lab1_Array {
    public static void main(String[] args){
        int[] array = new int[10];

        //Fills each element of array with the value = element's index squared
        for (int n = 0; n < 10; n++){
            array[n] = (int) (Math.pow(n, 2.0));
        }

        //Print the array
        for (int n = 0; n < 10; n++){
            System.out.println("Number " + n + ": " + array[n]);
        }

    }
}
